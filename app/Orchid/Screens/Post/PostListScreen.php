<?php

namespace App\Orchid\Screens\Post;

use App\Orchid\Layouts\Post\PostListLayout;
use App\Models\Post;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layout;
use Orchid\Screen\Screen;
use Orchid\Screen\TD;

class PostListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'My Forum';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'All blog posts';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'posts' => Post::latest()->paginate(10)
        ];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Create new')
                ->icon('icon-plus')
                ->route('platform.post.edit'),

            Link::make('Download')
                ->icon('icon-minus')
                ->route('platform.post.edit')

        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::table('posts', [

                TD::set('title', 'Title')
                    ->render(function (Post $post) {
                        return Link::make($post->title)
                            ->route('platform.post.edit', $post);
                    }),

                TD::set('created_at', 'Created'),
                TD::set('updated_at', 'Last edit'),
            ])
        ];
    }
}
