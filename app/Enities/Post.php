<?php

namespace App\Enities;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function upvotes(){
        return $this->hasMany(PostUpvote::class)->count();
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
