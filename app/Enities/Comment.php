<?php

namespace App\Enities;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function post(){
       return $this->belongsTo(Post::class); 
    }
    public function upvote(){
        return $this->hasMany(CommentUpvote::class)->count();
    }
    public function parentComment(){
        if($this->parent_id){
            return $this->belongsTo(Comment::class,'parent_id');
        }else{
            return 500;
        }        
    }
    public function replies(){
        return $this->hasMany(Comment::class,'parent_id','id');
    }
    public function user(){
        return $this->belongsTo(User::class);
    }
}
