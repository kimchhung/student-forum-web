<?php

namespace App\Entities;

use App\Enities\UserUpvoteCount;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public function post(){
        return $this->hasMany(Post::class);
    }
    public function upvote(){
        return $this->belongsTo(UserUpvoteCount::class);
    }
}
