<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as faker;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = faker::create();
        foreach (range(1, 100) as $index) {
            DB::table('posts')->insert([

                'id' => $index,
                'title' => $faker->sentence(2),
                'description' => $faker->paragraph(1),
                'body' => $faker->paragraph(4),
                'author' => '1',

            ]);
        }
    }
}
