<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::prefix('/user')->group(function () {
    Route::post('/login', 'Api\Auth\LoginController@login');
    Route::post('/register', 'Api\Auth\RegisterController@index');
    Route::middleware('auth:api')->get('/detail', 'Api\User\UserController@index');
});

Route::get('/api/get-posts', );
Route::get('/api/get-post/{post_id}');
Route::get('/api/get-comment/{post_id}');


